import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.beginTransaction();


        EntityManager entityManager = session.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        allListProducts(entityManager, criteriaBuilder);
        findProductsWherePrice(entityManager, criteriaBuilder);
        findProductsById(entityManager, criteriaBuilder);

        joinClients(entityManager, criteriaBuilder);


        session.getTransaction().commit();

        session.close();

    }

    private static void joinClients(EntityManager entityManager, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);
        Root<Product> from = criteriaQuery.from(Product.class);
        Join<Product, Client> join = from.join("client");
        criteriaQuery.select(join.get("name"));
        List<String> resultList = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println("Список продуктов клиентов: "+ resultList);
    }

    private static void findProductsById(EntityManager entityManager, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> from = criteriaQuery.from(Product.class);
        Predicate client_id = criteriaBuilder.equal(from.get("client"), 2);
        criteriaQuery.where(client_id);
        List<Product> resultList = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println("Список продуктов у клиента с айди 2: " + resultList);
    }

    private static void findProductsWherePrice(EntityManager entityManager, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> from = criteriaQuery.from(Product.class);
        criteriaQuery.select(from);
        Predicate price = criteriaBuilder.equal(from.get("price"), 110);
        criteriaQuery.where(price);
        List<Product> resultList = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println("Наш список продуктов с ценой 110 - " + resultList);
    }

    private static void allListProducts(EntityManager entityManager, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> from = criteriaQuery.from(Product.class);
        criteriaQuery.select(from);
        List<Product> resultList = entityManager.createQuery(criteriaQuery).getResultList();
        System.out.println("Наш список продуктов = " + resultList);
    }


}
